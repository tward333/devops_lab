# diagram.py
from diagrams import Cluster, Diagram, Edge
from diagrams.generic.compute import Rack
from diagrams.generic.blank import Blank
from diagrams.onprem.client import User
from diagrams.onprem.monitoring import Prometheus
from diagrams.onprem.monitoring import Thanos
from diagrams.onprem.monitoring import Grafana
from diagrams.onprem.network import Nginx
from diagrams.onprem.compute import Server
from diagrams.programming.language import Go
from diagrams.aws.storage import SimpleStorageServiceS3Bucket
from diagrams.saas.cdn import Cloudflare
from diagrams.custom import Custom
from urllib.request import urlretrieve

graph_attr = {
    "splines": "ortho",
    "concentrate":"false",
}
edge_attr = {
    "concentrate":"true",
}

#define gitlab


#build1.vmlocal     


#with Diagram("sysstat monitoring", show=False, graph_attr=graph_attr, direction="LR"):
with Diagram("devops_lab_libvirt", show=False, direction="LR", graph_attr=graph_attr):
#define custom gitlab icon
    gitlab_url = "https://about.gitlab.com/images/press/logo/png/gitlab-icon-rgb.png"
    gitlab_icon = "gitlab-icon-rgb.png"
    urlretrieve(gitlab_url, gitlab_icon)
#define custom bunnycdn icon
    bunnycdn_url = "https://ps.w.org/bunnycdn/assets/icon-256x256.png"
    bunnycdn_icon = "icon-256x256.png"
    urlretrieve(bunnycdn_url, bunnycdn_icon)
#define custom gocd icon
    gocd_url = "https://docs.gocd.org/current/svg/product_ui_logo_go.svg"
    gocd_icon = "product_ui_logo_go.svg"
    urlretrieve(gocd_url, gocd_icon)



    public =  User("Public")
#    bucket_deploy = SimpleStorageServiceS3Bucket("deployblob s3 bucket")

    with Cluster("Ops"):
      bucket_origin = SimpleStorageServiceS3Bucket("catorigin s3 bucket")
      cdn = Custom("bunny cdn", bunnycdn_icon)
      cloudflare = Cloudflare("cloudflare")
      with Cluster("Front-end"):
          ig1 = Nginx("ingress1")
          ig2 = Nginx("ingress2")
          content1 = Nginx("content1")
          content2 = Nginx("content2")
      with Cluster("Monitoring"):
          alert = Prometheus("Alertmanager")
          prometheus_mon = Prometheus("appmon")
          prometheus_selfmon = Prometheus("selfmon")
          grafana = Grafana("grafana")
    with Cluster("Dev"):
      build = Custom("GoCD build server", gocd_icon)
      gitlab = Custom("catapp repo", gitlab_icon)
      with Cluster("App pools"):
          blue = [Go("blue worker pool 1..N")]
          green = [Go("green worker pool 1..N")]



        

    #grafana - Edge(style="dashed", color="red", label="manual failover", minlen="0.1") - grafana2
    #grafana >> Edge(color="red", style="dashed", taillabel="fallback targets", minlen="2" ) >> prometheus_ceph 
    #prometheus_selfmon_sc >> alert >> Edge(style="dashed", color="red", minlen="2", headlabel="slack #prometheus-alertmanager") >> tward
##ops
    ig1 >> Edge(color="green", style="dashed", label="keepalived", minlen="2" ) >> ig2
    ig2 >> Edge(color="green", style="dashed",  minlen="2" ) >> ig1
    content1 >> Edge(color="green", style="dashed", label="keepalived", minlen="2" ) >> content2
    content2 >> Edge(color="green", style="dashed",  minlen="2" ) >> content1
    public >> cdn >> content1 >> bucket_origin
    public >> cloudflare >> ig1
    ig1 >> Edge( minlen="2", headlabel="active deployment"  ) >> blue[0] 
    ig1 >> Edge( minlen="2", headlabel="inactive deployment", style="dashed" ) >> green[0] 
    alert >> prometheus_mon   
    alert >> prometheus_selfmon
    grafana >> prometheus_mon
##dev
    build >> Edge(color="blue",  minlen="1" ) >> blue
    build >> Edge(color="green", minlen="1" ) >> green
    gitlab >>Edge(color="blue", taillabel="blue branch", minlen="2" ) >> build
    gitlab >>Edge(color="green", taillabel="green branch", minlen="2" ) >> build

