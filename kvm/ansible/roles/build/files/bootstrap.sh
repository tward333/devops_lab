rclone sync locals3:deployblob/build/serverBackups/ /var/lib/go-server/artifacts/serverBackups/

#backupdir="/var/lib/go-server/artifacts/serverBackups/backup_20211201-091936"
basedir="/var/lib/go-server/artifacts/serverBackups/"
latest=$(ls /var/lib/go-server/artifacts/serverBackups/|tail -1)
backupdir=$basedir$latest

systemctl stop go-server
mkdir -p /var/lib/go-server/db/h2db/
mkdir -p /var/lib/go-server/db/config.git/

unzip -o $backupdir/db.zip  -d /var/lib/go-server/db/h2db/
unzip -o $backupdir/config-dir.zip -d /etc/go/
unzip -o $backupdir/wrapper-config-dir.zip -d /usr/share/go-server/wrapper-config/
unzip -o $backupdir/config-repo.zip -d /var/lib/go-server/db/config.git/

chown -R go.go /var/lib/go-server/db/config.git/
chown -R go.go /usr/share/go-server/wrapper-config/
chown -R go.go /etc/go/
chown -R go.go /var/lib/go-server/db/h2db/
chown -R go.go /var/lib/go-server/artifacts

systemctl start go-server
systemctl start go-agent
