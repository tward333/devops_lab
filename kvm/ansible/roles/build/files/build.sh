for branch in $@
do
echo building "$branch"
go build  -o $branch $branch/catsite.go
echo copying "$branch" to s3
rclone copyto $branch/catsite locals3:/deployblob/catbinary/catsite-$branch
done

#echo bucket contents:
#rclone ls locals3:/deployblob/catbinary



