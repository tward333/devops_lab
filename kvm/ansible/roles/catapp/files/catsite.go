package main

import (
    "fmt"
    "net/http"
)

func main() {
    http.HandleFunc("/", HelloServer)
    http.ListenAndServe(":8888", nil)
}

func HelloServer(w http.ResponseWriter, r *http.Request) {
    //fmt.Fprintf(w, "cats go here", r.URL.Path[1:])
    fmt.Fprintf(w, "cats go here\n")
}
