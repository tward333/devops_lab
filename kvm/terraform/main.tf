terraform {
  required_version = ">= 0.14"
  required_providers {
    libvirt = {
      source = "dmacvicar/libvirt"
      version = ">=0.6.11"
    }
  }
}


#default must be defined
provider "libvirt" {
  uri   = "qemu+ssh://root@192.168.4.13/system"
}



#===getting the feel for modules===#
#ubuntu testing
#module "ubuntu" {
#  source = "./ubuntu"
#}

#rocky testing
#module "rocky" {
#  source = "./rocky"
#}

#===secret vars===#
# see variables.tf

#===catapp infra===#
module "ig1" {
 source = "./rocky2"
 hostname = "ig1.vmlocal"
 cpu = "2"
 vmIP = "192.168.5.81"
 role = "ingressproxy"
 passwdhash = "${var.passwd}"
}
module "ig2" {
 source = "./rocky2"
 hostname = "ig2.vmlocal"
 cpu = "2"
 vmIP = "192.168.5.82"
 role = "ingressproxy"
 passwdhash = "${var.passwd}"
}
module "app1" {
 source = "./rocky2"
 hostname = "app1.vmlocal"
 vmIP = "192.168.5.91"
 role = "catapp"
 passwdhash = "${var.passwd}"
 depends_on = [module.ig2, module.ig1]
}
module "app2" {
 source = "./rocky2"
 hostname = "app2.vmlocal"
 vmIP = "192.168.5.92"
 role = "catapp"
 passwdhash = "${var.passwd}"
 depends_on = [module.ig2, module.ig1]
}
module "app3" {
 source = "./rocky2"
 hostname = "app3.vmlocal"
 vmIP = "192.168.5.93"
 role = "catapp"
 passwdhash = "${var.passwd}"
 depends_on = [module.ig2, module.ig1]
}
module "app4" {
 source = "./rocky2"
 hostname = "app4.vmlocal"
 vmIP = "192.168.5.94"
 role = "catapp"
 passwdhash = "${var.passwd}"
 depends_on = [module.ig2, module.ig1]
}
module "content1" {
 source = "./rocky2"
 hostname = "content1.vmlocal"
 vmIP = "192.168.5.83"
 role = "originproxy"
 passwdhash = "${var.passwd}"
}
module "content2" {
 source = "./rocky2"
 hostname = "content2.vmlocal"
 vmIP = "192.168.5.84"
 role = "originproxy"
 passwdhash = "${var.passwd}"
}
module "mon" {
 source = "./rocky2"
 hostname = "mon.vmlocal"
 vmIP = "192.168.5.85"
 role = "mon"
 cpu = "4"
 passwdhash = "${var.passwd}"
 depends_on = [module.app1, module.app2, module.app3, module.app4]
}
module "build1" {
 source = "./rocky2"
 hostname = "build1.vmlocal"
 vmIP = "192.168.5.86"
 role = "build"
 cpu = "4"
 memoryMB = "${1024*4}"
 passwdhash = "${var.passwd}"
 depends_on = [module.app1, module.app2, module.app3, module.app4]
}
