# variables that can be overriden
variable "hostname" { default = "rocky1" }
variable "domain" { default = "home.local" }
variable "memoryMB" { default = 1024*1 }
variable "cpu" { default = 1 }


terraform {
  required_version = ">= 0.14"
  required_providers {
    libvirt = {
      source = "dmacvicar/libvirt"
      version = ">=0.6.11"
    }
  }
}
resource "libvirt_volume" "os_image" {
  name = "${var.hostname}-os_image"
  pool = "default"
  #source = "https://dl.rockylinux.org/pub/rocky/8/images/Rocky-8-GenericCloud-8.4-20210620.0.x86_64.qcow2"
  # the source  responds weirdly so I just downloaded it manually
  base_volume_name = "Rocky-8-GenericCloud-8.4-20210620.0.x86_64.qcow2"

  format = "qcow2"
}

# provider doesn't support native cloud-init, so an iso-shim is used 
resource "libvirt_cloudinit_disk" "commoninit" {
          name = "${var.hostname}-commoninit.iso"
          pool = "default"
          user_data = data.template_file.user_data.rendered
          network_config = data.template_file.network_config.rendered
}


data "template_file" "user_data" {
  template = file("${path.module}/cloud_init.cfg")
  vars = {
    hostname = var.hostname
    fqdn = "${var.hostname}.${var.domain}"
  }
}

data "template_file" "network_config" {
  template = file("${path.module}/network_config_static.cfg")
}


# Create the machine
resource "libvirt_domain" "domain-rocky" {
  name = var.hostname
  memory = var.memoryMB
  vcpu = var.cpu

  disk {
       volume_id = libvirt_volume.os_image.id
  }

#hard-coding mac for dhcp troubleshooting reasons
  network_interface {
    network_name   = "default"
    mac            = "52:54:00:6e:93:0a"
  }

  cloudinit = libvirt_cloudinit_disk.commoninit.id

  console {
    type        = "pty"
    target_port = "0"
    target_type = "serial"
  }

  graphics {
    type = "spice"
    listen_type = "address"
    autoport = "true"
  }
}


