
variable "passwd" {
  description = "root pw hash"
  type        = string
  sensitive   = true
}

variable "sshkey" {
  description = "starting ssh keys"
  type    = list(string)
}
