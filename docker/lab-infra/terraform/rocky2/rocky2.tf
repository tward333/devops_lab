# variables that can be overriden
variable "hostname" { default = "rocky1" }
variable "domain" { default = "home.local" }
variable "memoryMB" { default = 1024*1 }
variable "cpu" { default = 1 }
variable "vmIP" { default = "127.0.0.1" }
variable "role" { default = "catproxy" }
# vars pulled from secret
variable "passwdhash" { }




terraform {
  required_version = ">= 0.14"
  required_providers {
    libvirt = {
      source = "dmacvicar/libvirt"
      version = ">=0.6.11"
    }
  }
}
resource "libvirt_volume" "os_image" {
  name = "${var.hostname}-os_image"
  pool = "default"
  #source = "https://dl.rockylinux.org/pub/rocky/8/images/Rocky-8-GenericCloud-8.4-20210620.0.x86_64.qcow2"
  # the source  responds weirdly so I just downloaded it manually
  base_volume_name = "Rocky-8-GenericCloud-8.4-20210620.0.x86_64.qcow2"

  format = "qcow2"
} 
resource "null_resource" "ansible-post" {
  provisioner "remote-exec" {
    connection {
#      host = "${var.vmIP}" 
      host = "${var.hostname}" 
      user = "ff"
      timeout = "20m"
    }

    inline = ["echo 'connected!'"]
  }
  provisioner "local-exec" {
    working_dir = "../ansible"
#    command = "ANSIBLE_HOST_KEY_CHECKING=False ANSIBLE_PIPELINING=True ansible-playbook -u root -i '${var.vmIP},' '${var.role}.yml'"
#    command = "ANSIBLE_HOST_KEY_CHECKING=False ANSIBLE_PIPELINING=False ansible-playbook -u root -i '${var.vmIP},' '${var.role}.yml'"
#    command = "ANSIBLE_HOST_KEY_CHECKING=False ANSIBLE_PIPELINING=False ansible-playbook -u root -i '${var.hostname},' '${var.role}.yml'"
    command = "ANSIBLE_HOST_KEY_CHECKING=False ANSIBLE_PIPELINING=False ansible-playbook -u root -e @secrets_file.enc -i inventory --limit  '${var.hostname}' '${var.role}.yml'"
  }

}

# provider doesn't support native cloud-init, so an iso-shim is used 
resource "libvirt_cloudinit_disk" "commoninit" {
          name = "${var.hostname}-commoninit.iso"
          pool = "default"
#          user_data = data.template_file.user_data.rendered
          user_data = templatefile("${path.module}/cloud_init.cfg", {
                    hostname = var.hostname
                    fqdn = "${var.hostname}.${var.domain}"
                    passwd = var.passwdhash
		})
          network_config = data.template_file.network_config.rendered
}


#data "template_file" "user_data" {
#  template = file("${path.module}/cloud_init.cfg")
#  vars = {
#    hostname = var.hostname
#    fqdn = "${var.hostname}.${var.domain}"
#    passwd = var.passwdhash
##    sshkey = tostring(var.sshkeys)
##    sshkey = ["${sshkeys}"]
# sshkey = "${var.sshkeys}"
#
#  }
#}

data "template_file" "network_config" {
  template = file("${path.module}/network_config_static.cfg")
  vars = {
    vmIP = var.vmIP
  }

}


# Create the machine
resource "libvirt_domain" "domain-rocky" {
  name = var.hostname
  memory = var.memoryMB
  vcpu = var.cpu

  disk {
       volume_id = libvirt_volume.os_image.id
  }

  network_interface {
    network_name   = "default"
  }

  cloudinit = libvirt_cloudinit_disk.commoninit.id

  console {
    type        = "pty"
    target_port = "0"
    target_type = "serial"
  }

  graphics {
    type = "spice"
    listen_type = "address"
    autoport = "true"
  }
}


