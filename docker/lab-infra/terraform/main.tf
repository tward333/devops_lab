terraform {
  required_version = ">= 0.14"
  required_providers {
    libvirt = {
      source = "dmacvicar/libvirt"
      version = ">=0.6.11"
    }
  }
}


#default must be defined
provider "libvirt" {
  uri   = "qemu+ssh://root@192.168.4.13/system"
}





#===secret vars===#
# see variables.tf

#===catapp infra===#
module "docker1" {
 source = "./rocky2"
 hostname = "docker1.vmlocal"
 cpu = "4"
 memoryMB = "${1024*2}"
 vmIP = "192.168.5.71"
 role = "dockerhost"
 passwdhash = "${var.passwd}"
}
module "docker2" {
 source = "./rocky2"
 hostname = "docker2.vmlocal"
 cpu = "4"
 memoryMB = "${1024*2}"
 vmIP = "192.168.5.72"
 role = "dockerhost"
 passwdhash = "${var.passwd}"
 depends_on = [module.docker1]
}
module "docker3" {
 source = "./rocky2"
 hostname = "docker3.vmlocal"
 cpu = "4"
 memoryMB = "${1024*2}"
 vmIP = "192.168.5.73"
 role = "dockerhost"
 passwdhash = "${var.passwd}"
 depends_on = [module.docker1]
}
module "docker4" {
 source = "./rocky2"
 hostname = "docker4.vmlocal"
 cpu = "4"
 memoryMB = "${1024*2}"
 vmIP = "192.168.5.74"
 role = "dockerhost"
 passwdhash = "${var.passwd}"
 depends_on = [module.docker1]

}
