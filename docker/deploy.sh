echo -e "\n deploy"
#docker -H tcp://192.168.5.71:2376  stack deploy -c docker-compose.yml --with-registry-auth "tsman" --prune
#special pre-process to force docker-stack to use variables correctly
docker -H tcp://192.168.5.71:2376 stack deploy -c <(echo -e "version: '3.8'\n"; docker-compose --env-file .env config) --with-registry-auth "tsman" --prune
echo -e "\n services"
docker -H tcp://192.168.5.71:2376 stack services  "tsman"
echo -e "\n ps"
docker -H tcp://192.168.5.71:2376 stack ps --no-trunc  "tsman"
