terraform {
  required_version = ">= 0.14"
  required_providers {
    libvirt = {
      source = "dmacvicar/libvirt"
      version = ">=0.6.11"
    }
  }
}


#default must be defined
provider "libvirt" {
  uri   = "qemu+ssh://root@192.168.4.13/system"
}





#===secret vars===#
# see variables.tf

#===catapp infra===#
module "kube1" {
 source = "./rocky2"
 hostname = "kube1.vmlocal"
 cpu = "4"
 memoryMB = "${1024*2}"
 vmIP = "192.168.5.71"
 role = "kubehost"
 passwdhash = "${var.passwd}"
}
module "kube2" {
 source = "./rocky2"
 hostname = "kube2.vmlocal"
 cpu = "4"
 memoryMB = "${1024*2}"
 vmIP = "192.168.5.72"
 role = "kubehost"
 passwdhash = "${var.passwd}"
 depends_on = [module.kube1]
}
module "kube3" {
 source = "./rocky2"
 hostname = "kube3.vmlocal"
 cpu = "4"
 memoryMB = "${1024*2}"
 vmIP = "192.168.5.73"
 role = "kubehost"
 passwdhash = "${var.passwd}"
 depends_on = [module.kube1]
}

# module "kube4" {
# source = "./rocky2"
# hostname = "kube4.vmlocal"
# cpu = "4"
# memoryMB = "${1024*2}"
# vmIP = "192.168.5.74"
# role = "kubehost"
# passwdhash = "${var.passwd}"
# depends_on = [module.kube1]
#}
