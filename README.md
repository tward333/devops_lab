this repo is to showcase modern technologies used in a live deployment scenario

work in progress -- this file will get updates and diagrams as working infra-as-code is developed 


### upstream credits:
https://github.com/mingrammer/diagrams - diagram library for python

https://github.com/dmacvicar/terraform-provider-libvirt -  this makes it possible for terraform to create kvm instances

https://github.com/fabianlee/terraform-libvirt-ubuntu-examples - my templates are adapted from here

https://github.com/kubernetes-sigs/kubespray - quickly gets the base install for k8 functional
### objectives:
* all infra is deployed via code only and must work completely hands-off
* everything is monitored
* secret injection using existing integrations
* keep as many things persistent as possible
* leverage s3 (minio) storage as much as possible
* code deployment is automatic via a gitops pipeline for CI/CD
* blue/green strategy for easy failback
* do it 3 times - bare VMs, docker, and k8

### Bare VM infra diagram
![libvirt diagram](http://pubss.static.mojocloud.com/devops_lab_libvirt.png)

### Bare VM features
*  VMs are created using terraform + libvirt + cloud-init
*  Configs are deployed using ansible
*  Builds are triggered via gitlab postback and proxied to GoCD, 1 build pipeline for each branch (blue, green, production)
*  GoCD pipelines are persistent via s3 backup, and defined as code in the catapp gitlab repo
*  persistent local s3 bucket for blobs and other things not appropriate for git
*  persistent local s3 bucket for public content, proxied and backed by a CDN
*  auto-provision ssl certificates via letsencrypt and cloudflare dns challenge
*  ansible secrets handled via ansible vault (file)
*  terraform secrets handled via terraform secretsfile
*  keepalived load balancing/HA for app and cdn origin
*  prometheus node_exporters on every host
*  blackbox_exporter to report on service responses
*  alertmanager rules to act on specific service states
*  service map defined as code using python diagram https://github.com/mingrammer/diagrams

### Bare VM TODO
*  write ansible code to provision the hypervisor too
*  refactor prometheus persistence with a tsdb snapshot instead of datadir pull 
*  refactor secrets to use hashicorp vault instead of secretsfiles 

### docker-swarm VM features (so far)
*  VMs are created using terraform + libvirt + cloud-init
*  fully automatic provisioning of docker, swarm setup, and initial deployment using ansible
*  gitlab ci/cd using on-prem docker-in-docker runners to build, test, deploy images to docker swarm
*  persistent docker volumes backed up to object storage and auto-retrieved on provision

### k8 VM features (so far)
*  VMs are created using terraform + libvirt + cloud-init
*  fully operational k8 cluster installed via kubespray
